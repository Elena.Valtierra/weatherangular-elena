// code to make the HTTP call to the APIXU API
// It’s best practice to create an Angular service to make HTTP requests
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // import the HttpClient library into the apixu.service.ts file to make HTTP requests to the APIXU API from the apixu.service.ts file itself. 


// The decorating of the service as @Injectable allows you to inject this service within the constructor in weather.component.ts so that you can use it inside your component.
@Injectable({
  providedIn: 'root'
})
export class ApixuService {

  constructor(private http: HttpClient) { }

  getWeather(location: any) {
    return this.http.get(
      "https://api.openweathermap.org/data/2.5/weather?appid=2c09c47d15fe24e75c3005fc75cdfb5f&q=" + location
    );

  }
}
