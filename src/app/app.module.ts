import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { RouterModule } from '@angular/router';
import { allAppRoutes } from './routes';
import { ReactiveFormsModule } from '@angular/forms'; // To use a reactive form
import { HttpClientModule } from '@angular/common/http';
import { ApixuService } from './service/apixu.service';

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(allAppRoutes),
    ReactiveFormsModule, // To use a reactive form
    HttpClientModule
  ],
  providers: [ApixuService], // In Angular, if you want to use a service that you have created, you need to specify that service as a provider within your module.ts file. In this case, you’ve specified it as a provider within your entire application in app.module.ts.
  bootstrap: [AppComponent]
})
export class AppModule { }
