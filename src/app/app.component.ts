import { Component } from '@angular/core';
import { map } from 'rxjs'; // for the pipe map
import { WeatherService } from './service/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'weather';

  constructor(private weatherService: WeatherService) { }


  // I don't understaaaaaaaaaaand wtf
  ngOnInit() {
    this.weatherService.getWeather()
      .pipe(map(r => {

      }))
      .subscribe(r => {
        console.log(r)
      })
  }
}
