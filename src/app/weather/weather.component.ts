import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApixuService } from '../service/apixu.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  public weatherSearchForm!: FormGroup; // Variable file that will reference your FormGroup
  // The FormBuilder import into your constructor so that you can use it in your component.
  // need to add ! to initialize
  public weatherData: any; // to send to the HTML

  constructor(private formBuilder: FormBuilder,
    private apixuService: ApixuService) { }
  // By adding the formBuilder to the constructor, it creates an instance of the FormBuilder class, allowing you to use it within your component.

  // I must create the tree of elements within the form first before you bind it to the HTML. To achieve this, I need to ensure that you create your form elements in the ngOnInit hook inside your WeatherComponent (ngOnInit hook)
  ngOnInit(): void {
    this.weatherSearchForm = this.formBuilder.group({
      location: ['']
    });
  }

  sendToAPIXU(formValues: any) { // Cannot use frequently "any" as type 
    // console.log(formValues); // check that the formValues are correctly printed 
    this.apixuService
      .getWeather(formValues.location)
      // .subscribe(data => console.log(data)); // show in console retrieved information
      .subscribe(data => this.weatherData = data)
    console.log(this.weatherData);

  }

}
